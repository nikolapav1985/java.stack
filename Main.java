class Main{
    public static void main(String args[]){
        int count; // number of items to push on top of a stack
        int i;
        Integer item;
        Integer items[];
        Stack<Integer> st;
        count=Integer.parseInt(args[0]);
        st=new Stack<Integer>(count+1,0);
        items=new Integer[count+1];
        for(i=count;i>=0;i--){
            System.out.printf("Pushing onto a stack %d\n", i);
            item=i;
            st.push(items,item);
        }
        for(i=count;i>=0;i--){
            item=st.pop(items);
            System.out.printf("Popping from a stack %d\n", item);
        }
    }
}
/*
* EXAMPLE OUTPUT
* Pushing onto a stack 2
* Pushing onto a stack 1
* Pushing onto a stack 0
* Popping from a stack 0
* Popping from a stack 1
* Popping from a stack 2
*
* COMPILE INSTRUCTIONS
* javac Main.java
*
* RUN INSTRUCTIONS
* e.g. java Main 2
*/

