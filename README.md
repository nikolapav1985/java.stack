A stack implementation
----------------------

- a simple stack implementation in Java
- array based
- couple of operations supported, push and pop
- any data type supported through generics
- check comments for more details
