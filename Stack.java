class Stack<T>{ // array based stack implementation
    protected int capacity;
    protected int top;
    public Stack(int cp, int tp){ // initial stack info
        this.capacity=cp;
        this.top=tp;
    }
    public int getCapacity(){ // get capacity
        return this.capacity;
    }
    public int getTop(){ // get top
        return this.top;
    }
    public void setCapacity(int cp){ // set capacity
        this.capacity = cp;
    }
    public void setTop(int tp){ // set top
        this.top = tp;
    }
    public void push(T[] items, T item){ // push item onto a stack
        if(this.top>=this.capacity){ // stack full, do nothing
            return;
        }
        items[this.top++]=item;
    }
    public T pop(T[] items){ // pop item from a stack
        return items[--this.top];
    }
}
